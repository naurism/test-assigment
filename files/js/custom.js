$(document).ready(function() {
// setting an elements to change for each select value
    $.select = {
    'disc' : $('#size-fields, #size'),
    'book' : $('#weight-fields, #weight'),
    'furniture' : $('#dimensions-fields, #height, #width, #lenght')
  };
//  triggers to check value on select field change
  $('#type').change(function() { 
//  hide if not selected, removes required property from fields
    $.each($.select, function() { this.hide().prop('required',false); }); 
//  show if selected, adds required property to fields
    $.select[$(this).val()].show().prop('required',true);
  });
});