<?php
        header("Location: index.php"); 
        //gets data from json and puts into $products
        include 'products.php';
        //as items don't have ids in array there is a loop to search for item with the same SKU as recieved
        //increasing counter to know which item is looped
        $x=0;
        //loop to get all products
        foreach($products as $product){
            //loop to get all selected items SKUs
            foreach ((array) $_POST['skus'] as $sku) {
                //checking if current prduct SKU is equal with selected SKU
                if ( current($product) == $sku)  {
                //deleting object in case SKUs matches
                //decreasing counter as this product is being deleted and next one will have same number
                array_splice($products,$x,1); --$x;}
            }
            //increasing counter 
            ++$x;
        }
        //encodes array back to json format
        $json = json_encode($products, JSON_PRETTY_PRINT);
        //saves data in json file
        file_put_contents('products.json', $json);
?>