<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Junior test assigment</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="row head">
            <h2 class="heading">Product Add</h2>
            <div class="buttons">
                <form action="add.php" method="post" class="form-inline">
                    <button class="btn btn-default" onclick="history.go(-1);">Back</button>
                    <button type="submit" class="btn btn-default save">Save</button>
            </div>
        </div>
        <div class="fields form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-1" for="sku">SKU:<span class="redstar">*</span></label>
                <div class="col-sm-4">
                    <input pattern="[a-zA-Z0-9]+" required type="text" class="form-control" name="sku" placeholder="Enter SKU">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-1" for="name">Name:<span class="redstar">*</span></label>
                <div class="col-sm-4">
                    <input pattern="[a-zA-Z]+" required type="text" class="form-control" name="name" placeholder="Enter name">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-1" for="price">Price:<span class="redstar">*</span></label>
                <div class="col-sm-4">
                    <input required type="number" min="0.01" step="0.01" class="form-control" name="price" placeholder="Enter price">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-1" for="type">Type:<span class="redstar">*</span></label>
                <div class="col-sm-4">
                    <select required class="form-control" id="type" name="type">
                        <option value="">Select type</option>
                        <option value="disc">Disc</option>
                        <option value="book">Book</option>
                        <option value="furniture">Furniture</option>
                    </select>
                </div>
            </div>
            <div id="size-fields" style="display:none">
                <div class="form-group">
                    <label class="control-label col-sm-1" for="size">Size:<span class="redstar">*</span></label>
                    <div class="col-sm-4">
                        <input id="size" type="text" class="form-control" name="size" placeholder="Enter size">
                    </div>
                </div>
                <p>Quisque in leo ut leo luctus lacinia. Pellentesque consectetur erat vitae nisi feugiat aliquet.</p>
            </div>
            <div id="dimensions-fields" style="display:none">
                <div class="form-group">
                    <label class="control-label col-sm-1" for="height">Height:<span class="redstar">*</span></label>
                    <div class="col-sm-4">
                        <input id="height" type="text" class="form-control" name="height" placeholder="Enter height">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-1" for="width">Width:<span class="redstar">*</span></label>
                    <div class="col-sm-4">
                        <input id="width" type="text" class="form-control" name="width" placeholder="Enter width">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-1" for="lenght">Lenght:<span class="redstar">*</span></label>
                    <div class="col-sm-4">
                        <input id="lenght" type="text" class="form-control" name="lenght" placeholder="Enter lenght">
                    </div>
                </div>
                <p>Please provide dimensions in HxWxL format</p>
            </div>
            <div id="weight-fields" style="display:none">
                <div class="form-group">
                    <label class="control-label col-sm-1" for="weight">Weight:<span class="redstar">*</span></label>
                    <div class="col-sm-4">
                        <input id="weight" type="text" class="form-control" name="weight" placeholder="Enter weight">
                    </div>
                </div>
                <p>Aenean consectetur faucibus nisl, at tristique odio tempus vel.</p>
            </div>
            </form>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>