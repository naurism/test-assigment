<?php
        header("Location: index.php");   
        //gets data from json and puts into $products
        include 'products.php';
        //gets data from form and puts it into $data
        $data = array(
          'sku'=> $_POST['sku'],
          'name'=> $_POST['name'],
          'price'=> $_POST['price'],
          'type'=> $_POST['type'],
          'height'=> $_POST['height'],
          'width'=> $_POST['width'],
          'lenght'=> $_POST['lenght'],
          'weight'=> $_POST['weight'],
          'size'=> $_POST['size'],
        );
        //adds new product to array
        $products[] = $data;
        //encodes array back to json format
        $json = json_encode($products, JSON_PRETTY_PRINT);
        //saves data to json file
        file_put_contents('products.json', $json);       
?>