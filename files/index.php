<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Junior test assigment</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <div class="row head">
            <h2 class="heading">Product list</h2>
            <div class="buttons">
                <!-- Form for sending selected item skus to delete.php  -->
                <form action="delete.php" method="post" class="form-inline">
                    <select class="form-control" id="">
                        <option value="delete">Delete selected</option>
                    </select>
                    <button  type="submit" class="btn btn-default">Apply</button>
                    <a href="new.php" class="btn btn-default add">Add new product</a>
            </div>
        </div>
        <div class="list">
            <?php
            //gets data from json and puts into $products
            include 'products.php'; 
            //loop to get all products
            foreach($products as $product) :?>
                <div class="col-md-3">
                    <div class="box">
                        <div class="checkbox">
                            <label>
                                <!--checkbox for adding item sku to skus array for sending them to delete.php-->
                                <input type="checkbox" name="skus[]" value="<?php echo $product->sku ?>">
                            </label>
                        </div>
                        <ul>
                            <!--displaying product details-->
                            <li><?php echo $product->sku;?></li>
                            <li><?php echo $product->name;?></li>
                            <li><?php echo $product->price," $";?></li>
                            <!--checking what type product and showing relative details-->
                            <li><?php if ($product->type=="disc") echo $product->size;
                                    elseif ($product->type=="book") echo $product->weight;
                                    elseif ($product->type=="furniture") echo $product->height,"x",$product->width,"x",$product->lenght;?>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--closing loop-->
                <?php endforeach;?>
                    </form>
        </div>
    </div>
</body>
</html>